package vlcj.attempt.one;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import com.sun.jna.Native;
import com.sun.jna.NativeLibrary;

import uk.co.caprica.vlcj.binding.LibVlc;
import uk.co.caprica.vlcj.component.EmbeddedMediaPlayerComponent;
import uk.co.caprica.vlcj.runtime.RuntimeUtil;

public class UserInterface {
	
	private final EmbeddedMediaPlayerComponent mediaPlayerComponent;
	
	public static void main(final String[] args) {
		String vlcDir = "C:\\Program Files\\VideoLAN\\VLC";
		
		NativeLibrary.addSearchPath(RuntimeUtil.getLibVlcLibraryName(), vlcDir);
        Native.loadLibrary(RuntimeUtil.getLibVlcLibraryName(), LibVlc.class);
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new UserInterface(args);
            }
        });
	}
        
        private UserInterface(String[] args) {
    		String frameTitle = "VLCJ Attempt";
            JFrame frame = new JFrame(frameTitle);

            mediaPlayerComponent = new EmbeddedMediaPlayerComponent();

            frame.setContentPane(mediaPlayerComponent);

            frame.setLocation(100, 100);
            frame.setSize(1050, 600);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setResizable(true);
            frame.setVisible(true);

           mediaPlayerComponent.getMediaPlayer().playMedia(args[0]);
        }
}